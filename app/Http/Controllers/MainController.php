<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Item;

class MainController extends Controller
{
    public function index()
    {
        $items = Item::all();

        return view('main', compact('items'));
    }

    public function parse()
    {
        $client = new Client();
        $res = $client->request('GET', 'https://b2b-sandi.com.ua/api/products');

        $result= $res->getBody();
        $items = json_decode($result);

        foreach ($items as $item) {
            $newItem = new Item();
            $newItem->sku = $item->sku;
            $newItem->name_ru = $item->name_ru;
            $newItem->description_ru = $item->description_ru;
            $newItem->name_uk = $item->name_uk;
            $newItem->description_uk = $item->description_uk;
            $newItem->price = $item->price;
            $newItem->characteristics = json_encode($item->characteristics);
            $newItem->save();
        }

        return redirect()->route('main');
    }

    public function item($id)
    {
        $item = Item::findOrFail($id);

        return view('item', compact('item'));
    }
}
