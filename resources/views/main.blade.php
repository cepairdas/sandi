@extends('layouts.base')

@section('content')
<form action="{{ route('parse') }}" method="post">
    @csrf
    <input type="submit" value="@lang('message.get_project')">
</form>
@if (count($items) > 0)
    <table>
        <thead>
            <th>ID</th>
            <th>@lang('message.code')</th>
            <th>@lang('message.name')</th>
            <th>@lang('message.price')</th>
        </thead>
        <tbody>
            @foreach ($items as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->sku }}</td>
                    <td><a href="{{ route('item', ['id' => $item->id]) }}">{{ $item->{'name_' . LaravelLocalization::getCurrentLocale()} }}</a></td>
                    <td>{{ $item->price }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <p>@lang('message.no_products')</p>
@endif
@endsection
