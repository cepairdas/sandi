@extends('layouts.base')

@section('content')
    <table>
        <thead>
        <th>ID</th>
        <th>@lang('message.code')</th>
        <th>@lang('message.name')</th>
        <th>@lang('message.description')</th>
        <th>@lang('message.price')</th>
        </thead>
        <tbody>
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->sku }}</td>
            <td>{{ $item->{'name_' . LaravelLocalization::getCurrentLocale()} }}</td>
            <td>{{ $item->{'description_' . LaravelLocalization::getCurrentLocale()} }}</td>
            <td>{{ $item->price }}</td>
        </tr>
        </tbody>
    </table>
    <p>@lang('message.characteristics')</p>
    <table>
        <thead>
            <th>@lang('message.characteristics.name')</th>
            <th>@lang('message.characteristics.value')</th>
        </thead>
        <tbody>
            @foreach(json_decode($item->characteristics) as $characteristic)
                <tr>
                    <td>{{ $characteristic->{'name_' . LaravelLocalization::getCurrentLocale()} }}</td>
                    <td>{{ $characteristic->{'value_' . LaravelLocalization::getCurrentLocale()} }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
