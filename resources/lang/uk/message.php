<?php

return [
    'code' => 'Код',
    'name' => 'Iм\'я',
    'description' => 'Опис',
    'price' => 'Ціна',
    'characteristics' => 'Характеристики',
    'characteristics.name' => 'Назва',
    'characteristics.value' => 'Значення',
    'get_project' => 'Стягнути проект',
    'no_products' => 'Товарів немає ',
];
