<?php

return [
    'code' => 'Код',
    'name' => 'Имя',
    'description' => 'Описание',
    'price' => 'Цена',
    'characteristics' => 'Характеристики',
    'characteristics.name' => 'Имя',
    'characteristics.value' => 'Значение',
    'get_project' => 'Стянуть проект',
    'no_products' => 'Товаров нет',
];
