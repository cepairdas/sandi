<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
        'prefix' => LaravelLocalization::setLocale(),
        //'middleware' => ['localizationRedirect', 'localeSessionRedirect', 'localizationRedirect']
    ], function() {
        Route::get('/', 'MainController@index')->name('main');
        Route::get('/item/{id}', 'MainController@item')->name('item');
        Route::post('/parse', 'MainController@parse')->name('parse');
});
